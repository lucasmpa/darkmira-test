import React from 'react';
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';
import { Link } from "react-router-dom";
import CarouselCars from '../../components/carouselCars';
import IconNavigation from '../../components/iconNavigation';

const { Header, Content, Footer } = Layout;

export default function MainPage() {
  return (
    <Layout className="layout" >
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
          <Menu.Item key="1">
            Página Principal
            </Menu.Item>
          <Menu.Item key="2">
            <Link to="/agendamento/">Agendar Serviço</Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="/administracao/">Administração</Link>
          </Menu.Item>
        </Menu>
      </Header>
      <Content style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
        <CarouselCars />
        <IconNavigation />
      </Content>
      <Footer style={{ textAlign: 'center' }}>Car Manager ©2021 Created by Lucas Matheus</Footer>
    </Layout>
  );
}