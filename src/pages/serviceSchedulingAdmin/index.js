import React, { useEffect, useState } from 'react';
import { Button, Table } from 'antd';
import 'antd/dist/antd.css';
import './styles.css';
import { Link } from "react-router-dom";

import { FaPlus } from 'react-icons/fa'
import { AiFillEdit } from 'react-icons/ai'
import { BsFillTrashFill } from 'react-icons/bs'
import AddService from '../../components/addService'
import EditService from '../../components/editService'
import RemoveService from '../../components/removeService'
import { Layout, Menu } from 'antd';
import {
    DesktopOutlined,
    PieChartOutlined,
    UserOutlined,
} from '@ant-design/icons';
import { apiURL } from '../../utils/apiUrl';

const { Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

export default function ServiceShedulingAdmin() {
    const [addModal, setAddModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [selectedItem, setSelectedItem] = useState()
    const [data, setData] = useState([]);


    const columns = [
        {
            title: 'Nome',
            dataIndex: 'name'
        },
        {
            title: 'Descrição',
            dataIndex: 'description'
        },
        {
            title: 'Preço Base',
            dataIndex: 'basePrice'
        },
        {
            title: 'Data de Cadastro',
            dataIndex: 'registrationDate'
        },
    ];

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedItem(selectedRows)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            name: record.name,
        }),
    };

    function getServices() {
        return fetch(apiURL + 'service')
            .then(data => data.json())
    }

    useEffect(() => {
        let mounted = true;
        getServices()
            .then(items => {
                if (mounted) {
                    setData(items)
                }
            })
        return () => mounted = false;
    }, [])

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <AddService
                visible={addModal}
                onClose={() => setAddModal(false)}
            />
            <EditService
                visible={editModal}
                onClose={() => setEditModal(false)}
                selectedItem={selectedItem}
            />
            <RemoveService
                visible={deleteModal}
                onClose={() => setDeleteModal(false)}
                selectedItem={selectedItem}
            />
            <Sider collapsible >
                <Menu theme="dark" mode="inline">
                    <Menu.Item key="1" icon={<PieChartOutlined />}>
                        <Link to="/">
                            Página Principal
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<DesktopOutlined />}>
                        Agendar Serviço
                    </Menu.Item>
                    <SubMenu key="sub1" icon={<UserOutlined />} title="Administração">
                        <Menu.Item key="3">
                            <Link to="/administracao/agendar-servico">
                                Tipos de Atendimento
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Link to="/administracao/agendamento">
                                Agendamentos
                            </Link>
                        </Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Content style={{ padding: '0 50px' }}>
                    <div className="site-layout-content">
                        <div className="container-title">
                            Listagem de Serviços
                        <div className="container-action-buttons">
                                <Button
                                    type="primary"
                                    className="action-button add"
                                    onClick={() => { setAddModal(true) }} >
                                    <FaPlus />
                                </Button>
                                <Button
                                    type="primary"
                                    className="action-button edit"
                                    onClick={() => { setEditModal(true) }} >
                                    <AiFillEdit />
                                </Button>
                                <Button
                                    type="primary"
                                    className="action-button delete"
                                    onClick={() => { setDeleteModal(true) }} >
                                    <BsFillTrashFill />
                                </Button>
                            </div>

                        </div>
                        <Table columns={columns}
                            dataSource={data}
                            rowSelection={{
                                type: "radio",
                                ...rowSelection
                            }} />
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>Car Manager ©2021 Created by Lucas Matheus</Footer>
     </Layout>
        </Layout >
    );
}