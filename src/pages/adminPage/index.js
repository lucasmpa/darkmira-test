import React,{useState, useEffect} from 'react';
import 'antd/dist/antd.css';
import { Link } from "react-router-dom";
import { FaHandsHelping } from 'react-icons/fa'
import { GrSchedule } from 'react-icons/gr'
import { Layout, Menu, Table, Tag, Space } from 'antd';
import {
    DesktopOutlined,
    PieChartOutlined,
    UserOutlined,
} from '@ant-design/icons';
import axios from 'axios';
import { apiURL } from '../../utils/apiUrl';

const { Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;



export default function AdminPage() {
    const [data,setData] = useState()



 useEffect(() => {
        axios.get(apiURL + 'schedule/')
            .then(data => setData(data.data));
    }, [])

 const columns = [
        {
            title: 'Placa do veículo',
            dataIndex: 'licensePlate'
        },
        {
            title: 'Proprietário',
            dataIndex: 'owner'
        },
        {
            title: 'Atendimento',
            dataIndex: 'attendance'
        },
        {
            title: 'Data',
            dataIndex: 'date'
        },
        {
            title: 'Hora',
            dataIndex: 'hour'
        },
        {
            title: 'Status',
            dataIndex: 'status'
        },
    ];



    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Sider collapsible >
                <Menu theme="dark" mode="inline">
                    <Menu.Item key="1" icon={<PieChartOutlined />}>
                        <Link to="/">
                            Página Principal
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<DesktopOutlined />}>
                        <Link to="/agendamento">
                            Agendar Serviço
                        </Link>
                    </Menu.Item>
                    <SubMenu key="sub1" icon={<UserOutlined />} title="Administração">
                        <Menu.Item key="3">
                            <Link to="/administracao/agendar-servico">
                                Tipos de Atendimento
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Link to="/administracao/agendamento">
                                Agendamentos
                            </Link>
                        </Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Content style={{ margin: '0 16px' }}>

                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360, margin: "40px 0" }}>
                        <h2>Bem-Vindo a página de administração!</h2>
                        <p>Onde você deseja navegar?</p>
                        <Table columns={columns} dataSource={data} />

                        <div className="container-buttons">
                            <Link to="/administracao/agendar-servico">
                                <div className="buttons">
                                    <FaHandsHelping size={50} />
                                    <h4>Tipos de Atendimento</h4>
                                </div>
                            </Link>
                            <Link to="/administracao/agendamento">
                                <div className="buttons">
                                    <GrSchedule size={50} />
                                    <h4>Agendamentos</h4>
                                </div>
                            </Link>
                        </div>

                    </div>
                </Content>
                 <Footer style={{ textAlign: 'center' }}>Car Manager ©2021 Created by Lucas Matheus</Footer>
     </Layout>
        </Layout >
    );
}