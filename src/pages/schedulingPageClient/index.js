import React, { useState, useEffect } from "react";
import {
  Layout,
  Menu,
  Input,
  Row,
  Col,
  Button,
  message,
  DatePicker,
  TimePicker,
  Select,
} from "antd";
import "antd/dist/antd.css";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { apiURL } from "../../utils/apiUrl";

const { Header, Content, Footer } = Layout;
const { Option } = Select;

export default function SchedulingPageClient() {
  const [registeredServices, setRegisteredServices] = useState();
  const [licensePlate, setLicensePlate] = useState();
  const [owner, setOwner] = useState();
  const [attendance, setAttendance] = useState();
  const [description, setDescription] = useState();
  const [date, setDate] = useState();
  const [hour, setHour] = useState();
  const [redirectWhenFinished, setRedirectWhenFinished] = useState(false);

  const requestData = {
    licensePlate: licensePlate,
    owner: owner,
    attendance: attendance,
    description: description,
    date: currentDate(),
    hour: currentTime(),
    status: "Confirmado",
    key: Math.floor(Math.random() * 9999),
  };

  useEffect(() => {
    axios
      .get(apiURL + "service/")
      .then((data) => setRegisteredServices(data.data));
  }, []);

  console.log(registeredServices);
  function currentTime() {
    if (!hour) {
      var date = new Date(),
        currentHour = date.getHours(),
        currentMinutes = date.getMinutes(),
        currentSeconds = date.getSeconds();

      return currentHour + ":" + currentMinutes + ":" + currentSeconds;
    }
    return hour;
  }

  function currentDate() {
    if (!date) {
      let currentDate = new Date(),
        day = currentDate.getDate().toString().padStart(2, "0"),
        mounth = (currentDate.getMonth() + 1).toString().padStart(2, "0"),
        year = currentDate.getFullYear();

      return day + "/" + mounth + "/" + year;
    }
    return date;
  }

  function checkFields() {
    if (licensePlate && owner && attendance) {
      return true;
    }
    return false;
  }

  async function postService() {
    if (checkFields()) {
      return await axios
        .post(apiURL + "schedule/", requestData)
        .then(() =>
          message
            .loading("Carregando", 1.5)
            .then(() =>
              message.success("Agendamento Realizado com Sucesso", 2.5)
            )
        )
        .then(() => setRedirectWhenFinished(true));
    }
    return message.error(
      "Selecione todos os campos obrigatórios para continuar.",
      1.5
    );
  }

  function getHour(time, timeString) {
    setHour(timeString);
  }

  function getDate(time, dateString) {
    setDate(dateString);
  }

  function handleChange(value) {
    setAttendance(value);
  }

  return (
    <Layout className="layout">
      {redirectWhenFinished && <Redirect to="/" />}
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
          <Menu.Item key="1">
            <Link to="/">Página Principal</Link>
          </Menu.Item>
          <Menu.Item key="2">Agendar Serviço</Menu.Item>
          <Menu.Item key="3">
            <Link to="/administracao/">Administração</Link>
          </Menu.Item>
        </Menu>
      </Header>
      <div className="content-form">
        <Content className="site-layout-content client-content-layout ">
          <div className="title-container">
            <h2>Agendar Serviço</h2>
            <h5>* Campos Obrigatórios</h5>
          </div>
          <Row gutter={24}>
            <Col className="gutter-row" span={12}>
              Placa do carro <i style={{ color: "red" }}>*</i>
              <Input
                onChange={(e) => setLicensePlate(e.target.value)}
                placeholder="Ex: ABC-123"
              />
            </Col>
            <Col className="gutter-row" span={12}>
              Proprietário <i style={{ color: "red" }}>*</i>
              <Input
                onChange={(e) => setOwner(e.target.value)}
                placeholder="Ex: João da Silva"
              />
            </Col>
            <Col
              className="gutter-row"
              style={{ display: "flex", flexDirection: "column" }}
              span={12}
            >
              <span>
                Atendimento <i style={{ color: "red" }}>*</i>
              </span>
              <Select onChange={handleChange}>
                {registeredServices &&
                  registeredServices.map((data, index) => {
                    return (
                      <>
                        <Option value={data.name}>{data.name}</Option>
                      </>
                    );
                  })}
              </Select>
            </Col>
            <Col className="gutter-row" span={12}>
              Observação
              <Input onChange={(e) => setDescription(e.target.value)} />
            </Col>
            <Col
              className="gutter-row"
              style={{ display: "flex", flexDirection: "column" }}
              span={12}
            >
              Data
              <DatePicker
                format={"DD/MM/YYYY"}
                placeholder={"Selecione uma data"}
                getDate={getDate}
              />
            </Col>
            <Col
              className="gutter-row"
              style={{ display: "flex", flexDirection: "column" }}
              span={12}
            >
              Hora
              <TimePicker
                placeholder={"Selecione um horário"}
                format={"HH:mm"}
                onChange={getHour}
              />
            </Col>
          </Row>
          <Button
            type="primary"
            style={{ marginTop: "30px" }}
            onClick={() => postService()}
          >
            Agendar
          </Button>
        </Content>
      </div>
      <Footer style={{ textAlign: "center" }}>
        Car Manager ©2021 Created by Lucas Matheus
      </Footer>
    </Layout>
  );
}
