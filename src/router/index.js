import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import MainPage from '../pages/mainPage'
import SchedulingPageClient from '../pages/schedulingPageClient'
import SchedulingPageAdmin from '../pages/schedulingPageAdmin'
import TypeServices from '../pages/typeServices'
import AdminPage from '../pages/adminPage'
export default function Routes() {
  return (
    <Router>
      <Switch>
            <>
              <Route exact path="/" component={MainPage} />
              <Route exact path="/administracao" component={AdminPage} />
              <Route exact path="/agendamento" component={SchedulingPageClient} />
              <Route exact path="/administracao/agendar-servico" component={TypeServices} />
              <Route exact path="/administracao/agendamento" component={SchedulingPageAdmin} />
            </>
      </Switch>
    </Router>
  );
}
