import React, { useState } from 'react';
import { Modal, Input, Row, Col, message} from 'antd';
import axios from 'axios'
import 'antd/dist/antd.css';
import { apiURL } from '../../utils/apiUrl';


export default function AddService(props) {
    const { visible, onClose } = props;
    const [licensePlate, setLicensePlate] = useState();
    const [owner, setOwner] = useState();
    const [attendance, setAttendance] = useState();
    const [description, setDescription] = useState();
    const [date, setDate] = useState();
    const [hour, setHour] = useState();
    const [status, setStatus] = useState();

    function currentTime() {
        if (!hour) {
            var date = new Date(),
                currentHour = date.getHours(),
                currentMinutes = date.getMinutes(),
                currentSeconds = date.getSeconds();

            return currentHour + ':' +currentMinutes+ ':' +currentSeconds
        }
        return hour;
    }

    function currentDate() {
        if (!date) {
            var currentDate = new Date(),
                day = currentDate.getDate().toString().padStart(2, '0'),
                mounth = (currentDate.getMonth() + 1).toString().padStart(2, '0'),
                year = currentDate.getFullYear();

            return day + "/" + mounth + "/" + year;
        }
        return date;

    }
    const requestData = {
        licensePlate: licensePlate,
        owner: owner,
        attendance: attendance,
        description: description,
        date: currentDate(),
        hour: currentTime(),
        status: status,
        key: Math.floor(Math.random() * 9999)
    };
    function checkFields() {
        if (
          licensePlate &&
          owner &&
          attendance &&
          status
        ) {
          return true
        }
        return false
      }

    async function postService() {
        if(checkFields()){
            await axios.post(apiURL+ 'schedule/', requestData);
            window.location.reload();
        }
        return message.error('Preencha todos os campos obrigatórios');
       
    }

    return (
        <Modal  
            title="Adicionar Agendamento"
            visible={visible}
            width={700}
            onOk={() => postService()}
            onCancel={onClose}>

                <h5 className="obrigatory-fields">* Campos Obrigatórios</h5>

            <Row gutter={24}>
                <Col className="gutter-row" span={12}>
                    Placa do carro <i style={{ color: "red" }}>*</i>
                    <Input
                        onChange={(e) => setLicensePlate(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Proprietário <i style={{ color: "red" }}>*</i>
                    <Input
                        onChange={(e) => setOwner(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Atendimento
                    <Input
                        onChange={(e) => setAttendance(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Descrição <i style={{ color: "red" }}>*</i>
                    <Input
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Data
                    <Input
                        onChange={(e) => setDate(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Hora
                    <Input
                        onChange={(e) => setHour(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={24}>
                    Status <i style={{ color: "red" }}>*</i>
                    <Input
                        onChange={(e) => setStatus(e.target.value)}
                    />
                </Col>
            </Row>
        </Modal>
    );
}
