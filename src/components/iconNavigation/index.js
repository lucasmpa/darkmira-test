import React from 'react';
import { Link } from "react-router-dom";
import { BsTools } from 'react-icons/bs'
import { FaUserCog } from 'react-icons/fa'

const iconSize = 35;

export default function IconNavigation() {
  return (
    <div className="container">
      <div className="contentIcons">
        <Link to="/agendamento">
          <BsTools size={iconSize} className="icon" />
        </Link>
        <h3>Agendar Serviço</h3>
      </div>

      <div className="contentIcons">
        <Link to="/administracao">
          <FaUserCog size={iconSize} className="icon" />
        </Link>
        <h3>Administração</h3>
      </div>
    </div>
  );
}
