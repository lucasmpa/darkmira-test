import React, { useState } from "react";
import { Modal, Input, Row, Col, message } from "antd";
import axios from "axios";
import "antd/dist/antd.css";
import { apiURL } from "../../utils/apiUrl";

export default function AddService(props) {
  const { visible, onClose } = props;
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [basePrice, setBasePrice] = useState();

  function checkFields() {
    if (name && basePrice) {
      return true;
    }
    return false;
  }

  function date() {
    var date = new Date(),
      day = date.getDate().toString().padStart(2, "0"),
      mounth = (date.getMonth() + 1).toString().padStart(2, "0"),
      year = date.getFullYear();

    return day + "/" + mounth + "/" + year;
  }

  const requestData = {
    name: name,
    description: description,
    basePrice: basePrice,
    registrationDate: date(),
    key: Math.floor(Math.random() * 9999),
  };

  async function postService() {
    if (checkFields()) {
      await axios
        .post(apiURL + "service", requestData)
        .then(window.location.reload());
    }
    message.error("Preencha todos os dados para continuar.");
  }

  return (
    <Modal
      title="Adicionar Serviço"
      visible={visible}
      width={700}
      onOk={() => postService()}
      onCancel={onClose}
    >
      <Row gutter={24}>
        <Col className="gutter-row" span={12}>
          Nome
          <Input onChange={(e) => setName(e.target.value)} />
        </Col>
        <Col className="gutter-row" span={12}>
          Preço Base
          <Input
            onChange={(e) => setBasePrice(e.target.value)}
          />
        </Col>
      </Row>
      <Row gutter={24} style={{ marginTop: "20px" }}>
        <Col className="gutter-row" span={24}>
          Descrição
          <Input onChange={(e) => setDescription(e.target.value)} />
        </Col>
      </Row>
    </Modal>
  );
}
