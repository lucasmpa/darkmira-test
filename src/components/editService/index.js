import React, { useState } from 'react';
import { Modal, Input, Row, Col, message } from 'antd';
import axios from 'axios'
import 'antd/dist/antd.css';
import { apiURL } from '../../utils/apiUrl';

export default function EditService(props) {
    const { visible, onClose, selectedItem } = props;
    const [name, setName] = useState()
    const [description, setDescription] = useState()
    const [basePrice, setBasePrice] = useState()
    const [registrationDate, setRegistrationDate] = useState()

    const requestData = {
        name: name,
        description: description,
        basePrice: basePrice,
        registrationDate: registrationDate,
        key: Math.floor(Math.random() * 9999)
    };

    function checkFields() {
        if (
            name &&
            description &&
            basePrice &&
            registrationDate
        ) {
            return true
        }
        return false
    }

    async function putService() {
        if (selectedItem && checkFields()) {
            let id = selectedItem[0].id
            await axios.put(apiURL + 'service/' + id, requestData)
                .then(window.location.reload());
        }
        if (!selectedItem) {
            message.error('Você precisa selecionar o item que deseja alterar.');
        }
        if (!checkFields()) {
            message.error('Preencha todos os dados para continuar.');
        }
    }

    return (
        <Modal
            title="Editar Serviço"
            visible={visible}
            width={700}
            onOk={() => putService()}
            onCancel={onClose}>

            <Row gutter={24}>
                <Col className="gutter-row" span={12}>
                    Nome
                    <Input
                        onChange={(e) => setName(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Descrição
                    <Input
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Col>
            </Row>
            <Row gutter={24} style={{ marginTop: "20px" }} >
                <Col className="gutter-row" span={12}>
                    Preço Base
                    <Input
                        onChange={(e) => setBasePrice(e.target.value)}
                    />
                </Col>
                <Col className="gutter-row" span={12}>
                    Data de Cadastro
                    <Input
                        onChange={(e) => setRegistrationDate(e.target.value)}
                    />
                </Col>
            </Row>
        </Modal>
    );
}
