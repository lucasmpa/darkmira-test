import React from 'react';
import { Modal, message } from 'antd';
import axios from 'axios'
import 'antd/dist/antd.css';
import { apiURL } from '../../utils/apiUrl'


export default function RemoveSchedule(props) {
    const { visible, onClose, selectedItem } = props;

    async function removeService() {
        if (selectedItem) {
            let id = selectedItem[0].id
            await axios.delete(apiURL + 'schedule/' + id);
            window.location.reload();
        }
        if (!selectedItem) {
            message.error('Você precisa selecionar o item que deseja remover.');
        }
    }

    return (
        <Modal
            title="Remover Agendamento"
            visible={visible}
            width={700}
            onOk={() => removeService()}
            okType={'danger'}
            onCancel={onClose}>

            Você deseja mesmo excluir esse agendamento?
        </Modal>
    );
}
