import React from 'react';
import { Modal, message } from 'antd';
import axios from 'axios'
import 'antd/dist/antd.css';
import { apiURL } from '../../utils/apiUrl'


export default function RemoveService(props) {
    const { visible, onClose, selectedItem } = props;

    async function removeService() {
        if (selectedItem) {
            let id = selectedItem[0].id
            await axios.delete(apiURL + 'service/' + id);
            window.location.reload();
        }
        if (!selectedItem) {
            message.error('Você precisa selecionar o item que deseja remover.')
        }
    }

    return (
        <Modal
            title="Remover Serviço"
            visible={visible}
            width={700}
            onOk={() => removeService()}
            okType={'danger'}
            onCancel={onClose}>

            Você deseja mesmo excluir esse serviço?
        </Modal>
    );
}
