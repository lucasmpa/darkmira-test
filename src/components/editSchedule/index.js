import React, { useEffect, useState } from "react";
import { Modal, Input, Row, Col, message } from "antd";
import axios from "axios";
import "antd/dist/antd.css";
import { apiURL } from "../../utils/apiUrl";

export default function EditSchedule(props) {
  const { visible, onClose, selectedItem } = props;
  const [licensePlate, setLicensePlate] = useState();
  const [owner, setOwner] = useState();
  const [attendance, setAttendance] = useState();
  const [description, setDescription] = useState();
  const [date, setDate] = useState();
  const [hour, setHour] = useState();
  const [status, setStatus] = useState();

  const requestData = {
    licensePlate: licensePlate,
    owner: owner,
    attendance: attendance,
    description: description,
    date: date,
    hour: hour,
    status: status,
    key: Math.floor(Math.random() * 9999),
  };

  function checkFields() {
    if (licensePlate && owner && attendance && date && hour && status) {
      return true;
    }
    return false;
  }

  if (selectedItem) {
    licensePlate === undefined && setLicensePlate(selectedItem[0].licensePlate);
    owner === undefined && setOwner(selectedItem[0].owner);
    attendance === undefined && setAttendance(selectedItem[0].attendance);
    description === undefined && setDescription(selectedItem[0].description);
    date === undefined && setDate(selectedItem[0].date);
    hour === undefined && setHour(selectedItem[0].hour);
    status === undefined && setStatus(selectedItem[0].status);
  }

  async function putSchedule() {
    if (selectedItem && checkFields()) {
      let id = selectedItem[0].id;
      await axios
        .put(apiURL + "schedule/" + id, requestData)
        .then(window.location.reload());
    }
    if (!selectedItem) {
      message.error("Você precisa selecionar o item que deseja alterar.");
    }
    if (!checkFields()) {
      message.error("Preencha todos os campos obrigatórios.");
    }
  }

  return (
    <Modal
      title="Editar Agendamento"
      visible={visible}
      width={700}
      onOk={() => putSchedule()}
      onCancel={onClose}
    >
      <h5 className="obrigatory-fields">* Campos Obrigatórios</h5>

      <Row gutter={24}>
        <Col className="gutter-row" span={12}>
          Placa do carro <i style={{ color: "red" }}>*</i>
          <Input
            value={licensePlate}
            onChange={(e) => setLicensePlate(e.target.value)}
          />
        </Col>
        <Col className="gutter-row" span={12}>
          Proprietário <i style={{ color: "red" }}>*</i>
          <Input
            defaultValue={selectedItem && `${selectedItem[0].owner}`}
            onChange={(e) => setOwner(e.target.value)}
          />
        </Col>
        <Col className="gutter-row" span={12}>
          Atendimento <i style={{ color: "red" }}>*</i>
          <Input onChange={(e) => setAttendance(e.target.value)} />
        </Col>
        <Col className="gutter-row" span={12}>
          Observação
          <Input
            defaultValue={selectedItem && `${selectedItem[0].description}`}
            onChange={(e) => setDescription(e.target.value)}
          />
        </Col>
        <Col className="gutter-row" span={12}>
          Data
          <Input
            defaultValue={selectedItem && `${selectedItem[0].date}`}
            onChange={(e) => setDate(e.target.value)}
          />
        </Col>
        <Col className="gutter-row" span={12}>
          Hora
          <Input
            defaultValue={selectedItem && `${selectedItem[0].hour}`}
            onChange={(e) => setHour(e.target.value)}
          />
        </Col>
        <Col className="gutter-row" span={24}>
          Status <i style={{ color: "red" }}>*</i>
          <Input
            defaultValue={selectedItem && `${selectedItem[0].status}`}
            onChange={(e) => setStatus(e.target.value)}
          />
        </Col>
      </Row>
    </Modal>
  );
}
