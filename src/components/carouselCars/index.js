import React from 'react';

import { Carousel, Image } from 'antd';
import 'antd/dist/antd.css';

export default function CarouselCars() {
  return (
    <Carousel autoplay autoplaySpeed={4000} style={{maxWidth: '1200px'}}>
      <div>
        <Image
          width={'100%'}
          preview={false}
          src="https://www.chevrolet.com.br/content/dam/chevrolet/mercosur/brazil/portuguese/index/index-subcontent/43-images/masthead-bolt.jpg?imwidth=1200"
        />
      </div>
      <div>
        <Image
          width={'100%'}
          preview={false}
          src="https://www.chevrolet.com.br/content/dam/chevrolet/mercosur/brazil/portuguese/index/index-subcontent/43-images/s10-high-country-masthead-desk.jpg?imwidth=1200"
        />
      </div>
      <div>
        <Image
          width={'100%'}
          preview={false}
          src="https://www.chevrolet.com.br/content/dam/chevrolet/mercosur/brazil/portuguese/index/index-subcontent/41-images/masthead-novo-tracker-desktop.jpg?imwidth=1200"
        />
      </div>
    </Carousel>
  );
}
